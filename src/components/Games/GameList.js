import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchGames, fetchStreams } from '../../actions';
import './GameList.css'

class GameList extends Component {

    componentDidMount() {
        this.props.fetchGames();
    }

    handleGameSelect(gameID) {
        this.props.fetchStreams(gameID);
    }

    renderList() {
        return this.props.games.map((game) => {
            return (
                <div className={this.props.activeGame === game.id ? 'active-game game-item' : 'not-active-game game-item'}
                    onClick={() => this.handleGameSelect(game.id)} key={game.id}>
                    <img src={game.box_art_url.replace('{width}', '200').replace('{height}', '200')} alt={game.name} />
                    <h2>{game.name}</h2>
                    <hr />
                </div>
            );
        })
    }

    render() {
        return <div className='game-list'>{this.renderList()}</div>
    }
}

const mapStateToProps = (state) => {
    return { games: state.games, activeGame: state.streams.gameId }
}

export default connect(mapStateToProps, { fetchGames, fetchStreams })(GameList);