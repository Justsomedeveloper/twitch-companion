import React from 'react';
import Games from './Games/Games';
import Streams from './Streams/Streams';
import Header from './Header';
import './App.css'

const App = () => {
    return (
        <div className='complete-wrapper'>
            <Header />
            <div className="game-stream-wrapper">
                    <Games />
                    <Streams />
            </div>
        </div>
    )
}

export default App;