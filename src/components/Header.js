import React from 'react';
import './Header.css';


class Header extends React.Component {
    render() {
        return (
            <header className='header'>
                <a href='/' className='logo'><h2>Twitch Companion</h2></a>
                <nav className='site-nav'>
                    <ul>
                        <li>Home</li>
                        <li>Browse</li>
                        <li>Statistics</li>
                    </ul>
                </nav>
            </header>
        )
    }
}

export default Header;