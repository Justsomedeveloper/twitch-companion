import React from 'react';
import { connect } from 'react-redux';
import './ActiveStream.css';

class ActiveStream extends React.Component {
    render() {
        const { streamer } = this.props;
        if (!streamer) {
            return null
        }
        return (
            <div className='stream-video'>
                <iframe title='live-stream' src={`https://player.twitch.tv/?channel=${streamer}`} frameBorder="0" allowFullScreen="true" scrolling="no" height="500" width="100%"></iframe>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        streamer: state.streams.stream
    }
}

export default connect(mapStateToProps)(ActiveStream);