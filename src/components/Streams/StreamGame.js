import React from 'react';
import { connect } from 'react-redux';

class StreamGame extends React.Component{
    render() {
        const {game} = this.props;

        if(!game) {
            return null
        }
        return <p><i>{game.name}</i></p>
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        game: state.games.find((game) => game.id === ownProps.game)
    }
}

export default connect(mapStateToProps)(StreamGame);