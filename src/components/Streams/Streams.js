import React from 'react';
import StreamList from './StreamList';

const Streams = () => {
    return <StreamList />
}

export default Streams;