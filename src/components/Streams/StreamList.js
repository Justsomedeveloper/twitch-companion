import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchStreams, setActiveStream } from '../../actions';
import StreamGame from './StreamGame';
import './StreamList.css';
import ActiveStream from './ActiveStream';

class StreamList extends Component {

    componentDidMount() {
        this.props.fetchStreams();
    }

    handleStreamSelect(streamName) {
        this.props.setActiveStream(streamName);
    }

    renderList() {
        return this.props.streams.map((stream) => {
            return (
                <div className="stream-item" key={stream.id} onClick={() => this.handleStreamSelect(stream.user_name)}>
                    <img src={stream.thumbnail_url.replace('{width}', '300').replace('{height}', '200')} alt={stream.title} />
                    <h2>{stream.title}</h2>
                    <hr />
                    <h3>{stream.user_name}</h3>
                    <StreamGame game={stream.game_id} />
                </div>
            );
        })
    }

    render() {
        if (this.props.isLoading || this.props.streams === undefined) {
            return <h1>Loading...</h1>
        }
        return (
            <div className='stream-wrapper'>
                <ActiveStream />
                <div className='stream-list' style={{}}>
                    {this.renderList()}
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return { streams: state.streams.streams, isLoading: state.streams.isLoading }
}

export default connect(mapStateToProps, { fetchStreams, setActiveStream })(StreamList);