import { combineReducers } from 'redux';
import gamesReducer from './gamesReducer';
import streamsReducer from './streamsReducer';

export default combineReducers({
    games: gamesReducer,
    streams: streamsReducer
})