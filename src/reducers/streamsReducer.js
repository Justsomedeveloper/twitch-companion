export default (state = [] , action) => {
    switch(action.type) {
        case 'FETCH_STREAMS':
            return{streams: [], isLoading: true}
        case 'RECEIVE_STREAMS':
            return {streams: action.payload, isLoading: false, gameId: action.gameId}
        case 'SET_ACTIVE_STREAM':
            return {...state, stream: action.payload}
        default:
            return state;
    }
}