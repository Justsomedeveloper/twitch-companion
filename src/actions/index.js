import twitch from '../api/twitch';

export const fetchGames = () => {
    return async function (dispatch) {
        const response = await twitch.get('/games/top');

        dispatch({
            type: 'FETCH_GAMES',
            payload: response.data.data
        })
    }
}

export const fetchStreams = (gameId = null) => {
    return async function (dispatch) {
        let response = [];
        dispatch({
            type: 'FETCH_STREAMS'
        })

        if (gameId === null) {
            response = await twitch.get('/streams', 
            {
                params: {
                    first: '15'
                }
            })
        } else {
            response = await twitch.get('/streams',
                {
                    params: {
                        game_id: gameId,
                        first: '15'
                    }
                })
        }
        dispatch({
            type: 'RECEIVE_STREAMS',
            payload: response.data.data,
            gameId: gameId
        })


     
    }
}

export const setActiveStream = (streamName = null) => {
    return async function (dispatch) {
        dispatch({
            type: 'SET_ACTIVE_STREAM',
            payload: streamName
        })
    }
}