import axios from 'axios';

const KEY = process.env.REACT_APP_TWITCH_KEY
console.log(KEY)
export default axios.create({
    baseURL: 'https://api.twitch.tv/helix',
    headers: {
        'client-id': KEY
    }

})